#!/bin/bash
# install java git docker-engine mysql
yum install java-1.8.0-openjdk-devel.x86_64 -y
yum install git -y
yum install docker-engine.x86_64 -y
yum install mysql -y
# git clone 
git clone -b master-dev https://gitee.com/HuaweiCloudDeveloper/saas-housekeeper.git
# install maven
cd || exit
cd /usr/lib || exit
mkdir maven
cd maven || exit
wget https://repo.huaweicloud.com/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
tar -zxvf apache-maven-3.6.3-bin.tar.gz
# change mirror
sed -i "158a  <mirror>" /usr/lib/maven/apache-maven-3.6.3/conf/settings.xml
sed -i "159a <id>alimaven</id>" /usr/lib/maven/apache-maven-3.6.3/conf/settings.xml
sed -i "160a <mirrorOf>central</mirrorOf>" /usr/lib/maven/apache-maven-3.6.3/conf/settings.xml
sed -i "161a  <name>aliyun maven</name>" /usr/lib/maven/apache-maven-3.6.3/conf/settings.xml
sed -i "162a <url>http://maven.aliyun.com/nexus/content/repositories/central/</url>" /usr/lib/maven/apache-maven-3.6.3/conf/settings.xml
sed -i "163a </mirror>" /usr/lib/maven/apache-maven-3.6.3/conf/settings.xml
echo "export MAVEN_HOME=/usr/lib/maven/apache-maven-3.6.3" >> /etc/profile
echo 'export PATH=$PATH:$MAVEN_HOME/bin' >> /etc/profile
source /etc/profile
# install nodejs
cd || exit
cd /usr/lib || exit
mkdir node
cd node || exit
wget https://nodejs.org/dist/v16.16.0/node-v16.16.0-linux-x64.tar.xz
tar xf node-v16.16.0-linux-x64.tar.xz  
ln -s /usr/lib/node/node-v16.16.0-linux-x64/bin/npm   /usr/local/bin/ 
ln -s /usr/lib/node/node-v16.16.0-linux-x64/bin/node   /usr/local/bin/ 
# change mirror
npm install -g cnpm --registry=https://registry.npm.taobao.org
ln -s /usr/lib/node/node-v16.16.0-linux-x64/bin/cnpm   /usr/local/bin/cnpm 
# pack the backend modules
$1
cd || exit
cd saas-housekeeper || exit
module_list=("config-server" "eureka" "gateway" "message-service" "order-service" "service-publish-service" "task-service" "tenantinfo-service" "userinfo-service")
for value in ${module_list[@]}
do
    cd saas-housekeeper-$value || exit
    mvn clean install -DskipTests
    docker build -t 'saas-housekeeper-'$value:v-0.1 .
    sudo docker tag saas-housekeeper-$value:v-0.1 $2/$3/saas-housekeeper-$value:v-0.1 
    sudo docker push $2/$3/saas-housekeeper-$value:v-0.1 

cd .. || exit
done
# config the nginx
cd || exit
cd saas-housekeeper || exit
sed -i '46d' conf/nginx.conf
sed -i "45a server_name $4;" conf/nginx.conf
# pack the frontend modules
sh buildfront.sh
docker build -t 'front':v-0.1 .
sudo docker tag front:v-0.1 $2/$3/front:v-0.1 
sudo docker push $2/$3/front:v-0.1