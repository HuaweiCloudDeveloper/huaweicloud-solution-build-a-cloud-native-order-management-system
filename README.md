[TOC]

**解决方案介绍**
===============
该解决方案可以帮助您在华为云云容器引擎上，快速搭建订单管理系统，通过云容器引擎部署轻便及快速伸缩的能力，有效实现降本增效。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/build-a-cloud-native-order-management-system.html

**架构图**
---------------
![方案架构](./document/build-a-cloud-native-order-management-system.png)

**架构描述**
---------------
该解决方案部署如下资源：

1.创建云容器引擎CCE，用于搭建订单管理系统环境。

2.创建两个云容器引擎节点，跨AZ部署，用于创建工作负载。

3.创建一个云数据库RDS for MySQL，跨AZ主备部署，用于订单管理系统数据存储。

4.创建一个RabbitMQ节点，用于微服务模块间消息传递。

5.创建一个弹性负载均衡ELB并绑定到前端web模块，提供负载均衡能力。

6.创建一个弹性公网IP并绑定到弹性负载均衡ELB，提供被公网访问能力。

**组织结构**
---------------
``` lua
huaweicloud-solution-build-a-cloud-native-order-management-system
├── build-a-cloud-native-order-management-system.tf.json -- 资源编排模板
├── userdata
    ├── config.sh -- 脚本配置文件
```
**开始使用**
---------------
1、您可以访问订单管理系统域名，以超级管理员身份进行登录。例如，域名为 “solutionascode.cn”，记录集添加解析为 “one.saas-housekeeper.cloudbu.solutionascode.cn”， 则输入网址为  http://one.saas-housekeeper.cloudbu.solutionascode.cn/super-admin/#/login 。 超级管理员账号为Admin ，密码为lhKk101@mm. 。

图1 登录超级管理中心

![ 登录超级管理中心](./document/readme-image-001.png)

2、您可以访问租户注册页面。输入网址例如 http://one.saas-housekeeper.cloudbu.solutionascode.cn/tenant/#/register ，即可申请创建租户。

图2 登录租户注册界面

![登录租户注册界面](./document/readme-image-002.png)

3、申请通过后，租户可通过域名登录租户管理中心。假设租户域名为sac，则租户管理中心登录网址为 http://sac.saas-housekeeper.cloudbu.solutionascode.cn/tenant/#/login 。租户账号为tenant ，密码为lhKk101@mm.  。点击左上角“新建服务”，即可发布订单。

图3 登录租户管理中心

![登录租户管理中心](./document/readme-image-003.png)

4、用户可通过域名登录用户管理中心。假设租户域名为sac，则用户管理中心登录网址为 http://sac.saas-housekeeper.cloudbu.solutionascode.cn/customer/#/login 。用户可以选择订单购买工人服务。

图4 登录用户管理中心

![登录用户管理中心](./document/readme-image-004.png)

5、每个租户名下有数名工人，工人可通过域名登录工人管理中心。假设租户域名为sac，则工人管理中心登录网址为 http://sac.saas-housekeeper.cloudbu.solutionascode.cn/worker/#/login 。

图5 登录工人管理中心

![登录工人管理中心](./document/readme-image-005.png)



